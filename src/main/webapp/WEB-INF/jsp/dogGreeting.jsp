<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form commandName="formdogGreeting">
   <table>
   
    <tr>
        <td><form:label id="name" path="name">Content</form:label></td>
        <td><form:input id="name" path="name" /></td>
    </tr>
    <tr>
        <td><form:label id="breed" path="breed">Content</form:label></td>
        <td><form:input id="breed" path="breed" /></td>
    </tr>
    <tr>
        <td><form:label id="age" path="age">Content</form:label></td>
        <td><form:input id="age" path="age" /></td>
    </tr>
    
   
    <tr>
        <td colspan="2">
            <input type="submit" value="Woof"/>
        </td>
    </tr>
</table>  
</form:form>