<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div>
	
		The Cat's name is ${ dogGreeting.name }
		The Cat's breed is ${ dogGreeting.breed }
		The Cat's age is ${dogGreeting.age} 

</div>
<div class="row">
		<table class="table">
			<tr>
				<td>Name:</td>
				<td>${dogGreeting.name}</td>
			</tr>
			<tr>
				<td>Breed:</td>	
				<td>${dogGreeting.breed}</td>
			</tr>
			<tr>
				<td>Age:</td>
				<td>${dogGreeting.age}</td>
			</tr>
		</table>
	</div>
</body>
</html>