package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Greeting;
import com.metlife.dpa.model.dogGreeting;

@Controller
public class dogController {

	@RequestMapping(value="/dog")
	public String sayHello(Model model){
		model.addAttribute("dogGreeting", "Hello world!");
		return "index";
	}
	
    @RequestMapping(value="/dog", method=RequestMethod.GET)
    public String greetingForm(Model model) {
    	dogGreeting doggreet = new dogGreeting();
    	doggreet.setName("Please Input Cat Name");
    	doggreet.setBreed("Please Input Cat Breed");
    	doggreet.setAge("Please Input Cat Age");
        model.addAttribute("formdogGreeting", doggreet);
        return "dogGreeting";
        
    }

    
    @RequestMapping(value="/dog", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute dogGreeting dogGreeting, Model model) {
    	System.out.println(dogGreeting.getName());
    	System.out.println(dogGreeting.getBreed());
    	System.out.println(dogGreeting.getAge());
        model.addAttribute("dogGreeting", dogGreeting);
        return "dogresult";
    }
	
}
