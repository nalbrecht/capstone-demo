package com.metlife.dpa.model;

import java.math.BigDecimal;

public class Product {
	String name;
	BigDecimal price;
	
	public Product(String name, BigDecimal price){
		this.name = name;
		this.price = price;
	}
	// for model binding
	public Product(){}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	} 
}
